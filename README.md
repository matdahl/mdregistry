# MDRegistry

This project provides images used throughout my projects for pipelines etc.

Images are build and pushed into the container registry `registry.gitlab.com/matdahl/mdregistry`.
